#include <iostream>

#include <boost/asio/serial_port.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/system/error_code.hpp>
#include <boost/asio/buffer.hpp>
#include <boost/bind/bind.hpp>

#include <string>
#include <string_view>
#include <memory>
#include <iomanip>

class Main {
    boost::asio::io_context ioc;
    std::string str;
    boost::asio::dynamic_string_buffer<char,std::char_traits<char>, std::allocator<char>> str_buffer;
    boost::asio::serial_port serial;

    void read_callback(const boost::system::error_code &err, std::size_t size) {
        using std::cout;
        using std::cerr;
        using std::endl;
        using std::string;
        using std::string_view;
        using std::quoted;

        if (err) {
            cerr << err.message() << endl;
            return;
        }

        string::const_iterator line_begin = str.cbegin();
        string::const_iterator line_end = line_begin + size;

        cout << quoted(string_view(line_begin, line_end)) << endl;

        str.erase(0, size);

        init_async_read();
    }

    void init_async_read() {
        using boost::asio::async_read_until;
        using boost::bind;
        using namespace boost::placeholders;

        async_read_until(serial, str_buffer, "\r\n", bind(&Main::read_callback, this, _1, _2));
    }

public:
    Main(const std::string &serial_port_name)
        : ioc()
        , str()
        , str_buffer(str)
        , serial(ioc, serial_port_name)
    {
        using boost::asio::serial_port;

        serial.set_option(serial_port::baud_rate(115200));
    }

    void operator()() {
        init_async_read();
        ioc.run();
    }
};

int main(int argc, char *argv[])
{
    using std::cerr;
    using std::cout;
    using std::endl;

    if (argc < 2) {
        cerr << "Usage: " << argv[0] << " PORT" << endl;
        return 1;
    }

    Main main(argv[1]);
    main();
    return 0;
}
